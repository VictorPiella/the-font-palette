import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FontUploaderComponent } from './font-uploader/font-uploader.component';
import { FontGoogleComponent } from './font-google/font-google.component';


const routes: Routes = [
  { path: 'upload', component: FontUploaderComponent },
  { path: 'google', component: FontGoogleComponent },
  { path: '', redirectTo: '/upload', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
