import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Observable } from 'rxjs';
import { map, startWith, flatMap } from 'rxjs/operators';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import gFontJSON from '../../assets/googleFonts.json';

export interface Font {
  name: string,
  url: string
}

@Component({
  selector: 'app-font-google',
  templateUrl: './font-google.component.html',
  styleUrls: ['./font-google.component.scss']
})

export class FontGoogleComponent implements OnInit {
  previewText: string = "ABCDEFGHIJKLMNOPQKRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz\n0123456789\n! “” # · $ % & / ( ) = ?¿ @ # ’ ^ *¨ç [ ] < >` ´ { }";
  fonts: any[] = [];

  gForm = this.fb.group({
    googleFont: [''],
    customText: [''],
    textSize: [20]
  });
  gFontsJson = [];
  gFonts: Font[] = [];

  myControl = new FormControl<string>('');

  filteredOptions$: Observable<Font[]> = new Observable<Font[]>;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.gFonts = gFontJSON.flatMap((font: any) => this.getFontUrls(font));

    this.filteredOptions$ = this.gForm.controls.googleFont.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || ''))
    );
  }

  private _filter(value: string): Font[] {
    let filterValue = '';
    if (typeof value === 'string') {
      filterValue = value.toLowerCase();
    }
    return this.gFonts.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  getFontUrls(fontData: any) {
    const fontUrls = fontData.variants.map((variant: any) => {
      const fontName = `${fontData.family} ${variant}`;
      const url = fontData.files[variant];
      return { name: fontName, url };
    });
    return fontUrls;
  };


  onSelected(event: MatAutocompleteSelectedEvent) {
    const fontFace = new FontFace(event.option.value.name, `url(${event.option.value.url})`);
    (document.fonts as any).add(fontFace);
    fontFace.load().then(() => {
      this.fonts.push(event.option.value);
    });
    this.gForm.controls.googleFont.reset();
  }

  deleteFont(index: number): void {
    this.fonts.splice(index, 1);
  }

  onFileSelected(event: any): void {
    const files: any[] = event.target.files;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fonts, event.previousIndex, event.currentIndex);
  }
}
