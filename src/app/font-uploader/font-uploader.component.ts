import { Component } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-font-uploader',
  templateUrl: './font-uploader.component.html',
  styleUrls: ['./font-uploader.component.scss']
})
export class FontUploaderComponent {
  fontFamily: any;
  previewText: string = "ABCDEFGHIJKLMNOPQKRSTUVWXYZ\nabcdefghijklmnopqrstuvwxyz\n0123456789\n! “” # · $ % & / ( ) = ?¿ @ # ’ ^ *¨ç [ ] < >` ´ { }";
  fontList: any = [];
  fonts: any[] = [];

  constructor(private fb: FormBuilder) { }

  fontForm = this.fb.group({
    formPreviewText: [''],
    formPreviewFontSize: [20],
  });

  deleteFont(index: number): void {
    this.fonts.splice(index, 1);
  }

  getTitle(text:string) {
    const data:any = text.replace(/(-|\.)+/g, ' ');
    return data.slice(0,-4);
  }

  onFileDropped($event: any) {
    this.prepareFilesList($event);
  }

  fileBrowseHandler(files: any | null) {
    this.prepareFilesList(files.files);
  }
  
  onFileSelected(event: any): void {
    const files: any[] = event.target.files;
  }
  
  prepareFilesList(files: Array<any>) {

    for (let i = 0; i < files.length; i++) {
      const file: any = files[i];
      const extension = file.name.split('.').pop().toLowerCase();
      if (extension !== 'ttf' && extension !== 'otf') {
        alert("Only TTF and OTF files are allowed.");
        return;
      }

      const reader:any = new FileReader();
      reader.onload = () => {
        const fontUrl = reader.result.toString();
        const fontFamily = `custom-font-${Date.now()}`;
        const fontFace = new FontFace(fontFamily, `url(${fontUrl})`);
        (document.fonts as any).add(fontFace);
        fontFace.load().then(() => {
          this.fonts.push({ fontFamily, fileName: file.name });
        });
      };

      reader.readAsDataURL(file);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fonts, event.previousIndex, event.currentIndex);
  }
}
